package com.paytm

import org.apache.log4j.LogManager
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.{UserDefinedFunction, Window}
import org.apache.spark.sql.functions._

object Challenge {

    private val LOGGER = LogManager.getLogger(Challenge.getClass)

    val countryListCSVPath = "/Users/sonali/paytm-coding-challenge/data/countrylist.csv"
    val stationListCSVPath = "/Users/sonali/paytm-coding-challenge/data/stationlist.csv"
    val weatherDataPath = "/Users/sonali/paytm-coding-challenge/data/2019/"

    val getYearFromTimeUDF: UserDefinedFunction = udf((time: Integer) => {
        val timeString = time.toString
        timeString.substring(0, 4)
    })

    def main(args : Array[String] ) = {

        LOGGER.info("Setting up Spark Session")

        //println("Setting up Spark Session")
        val spark = SparkSession
                    .builder()
                    .master("local[1]" )
                    .getOrCreate()

        LOGGER.info("Reading country list")
        val countryList = spark.read.option("header",true).csv(countryListCSVPath)
        countryList.printSchema()

        LOGGER.info("Reading station list")
        val stationList = spark.read.option("header",true).csv(stationListCSVPath)
        stationList.printSchema()

        LOGGER.info("Reading weather data")
        val weatherDataDF = spark.read.option("header",true).csv(weatherDataPath)
        weatherDataDF.printSchema()

        LOGGER.info("Cleaning/renaming weather data")
        val weatherDataCleanedDF =   weatherDataDF.withColumn("STN_NO", weatherDataDF("STN---"))
                                          .withColumn("YEAR", getYearFromTimeUDF(weatherDataDF("YEARMODA")))
                                          .withColumn("WIND_SPEED", when(weatherDataDF("WDSP")===999.9, lit(null)).otherwise(weatherDataDF("WDSP")))
                                          .withColumn("TEMPERATURE", when(weatherDataDF("TEMP")===9999.9, lit(null)).otherwise(weatherDataDF("TEMP")))
                                          .select(
                                              col("STN_NO"),
                                              col("YEAR"),
                                              col("TEMPERATURE"),
                                              col("WIND_SPEED"),
                                              col("FRSHTT"))

        LOGGER.info("number of resords: "+weatherDataCleanedDF.count())
        weatherDataCleanedDF.printSchema()

        LOGGER.info("Joining Weather, Country and Station DataFrames")
        val joinedTableDF = weatherDataCleanedDF
          .join(stationList, "STN_NO")
          .join(countryList, "COUNTRY_ABBR")
          .persist()

        LOGGER.info("number of resords: "+joinedTableDF.count())
        joinedTableDF.printSchema()
//        joinedTableDF.show()


        LOGGER.info("Preparing list of avg temperatures for each country")
        val avgMeanTemp = joinedTableDF
          .groupBy("COUNTRY_FULL", "YEAR")
          .agg(avg("TEMPERATURE").as("AVG_TEMP"))


        LOGGER.info("========  1. Which country had the hottest average mean temperature over the year?  =======")
        //        1. Which country had the hottest average mean temperature over the year?
        avgMeanTemp
          .orderBy(desc("AVG_TEMP"))
          .show(1)
//        +------------+----+-----------------+
//        |COUNTRY_FULL|YEAR|         AVG_TEMP|
//        +------------+----+-----------------+
//        |    DJIBOUTI|2019|90.06114457831325|
//        +------------+----+-----------------+


        LOGGER.info("========  2. Which country had the most consecutive days of tornadoes/funnel cloud\nformations?  =======")
        //        2. Which country had the most consecutive days of tornadoes/funnel cloud formations?
        avgMeanTemp
          .orderBy(asc("AVG_TEMP")).show(1)
//        +------------+----+-------------------+
//        |COUNTRY_FULL|YEAR|           AVG_TEMP|
//        +------------+----+-------------------+
//        |  ANTARCTICA|2019|-2.8328838863485206|
//        +------------+----+-------------------+


        LOGGER.info("========  3. Which country had the second highest average mean wind speed over the year?  =======")
        //        3. Which country had the second highest average mean wind speed over the year?
        val windowSpec  = Window.orderBy(desc("AVG_WIND_SPEED"))
        joinedTableDF.groupBy("COUNTRY_FULL", "YEAR")
          .agg(avg("WIND_SPEED").as("AVG_WIND_SPEED"))
          .withColumn("row_number",row_number.over(windowSpec))
          .filter(col("row_number") === 2)
          .show()
//        +------------+----+--------------+----------+
//        |COUNTRY_FULL|YEAR|AVG_WIND_SPEED|row_number|
//        +------------+----+--------------+----------+
//        |     BERMUDA|2020|          16.9|         2|
//        +------------+----+--------------+----------+


    }
}
